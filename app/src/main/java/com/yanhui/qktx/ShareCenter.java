package com.yanhui.qktx;


import android.app.Activity;

import net.qktianxia.component.share.base.IShareCallBack;
import net.qktianxia.component.share.base.Interceptor;
import net.qktianxia.component.share.base.SharePlatform;
import net.qktianxia.component.share.base.IShareContentProvider;
import net.qktianxia.component.share.wechat.handler.UseIntentShareHandler;


/**
 * Created by xiaolong on 2018/8/31.
 * email：xinxiaolong123@foxmail.com
 */

public class ShareCenter {

    public static void toShare(Activity context, final IShareContentProvider shareContent, SharePlatform platform, final IShareCallBack callBack){

        ShareHandlerFactory.create(platform).toShare(context, platform, shareContent, new IShareCallBack() {
            @Override
            public void onStart(SharePlatform platform) {
                callBack.onStart(platform);
                mShareCallBack.onStart(platform);
            }

            @Override
            public void onResult(SharePlatform platform) {
                callBack.onResult(platform);
                mShareCallBack.onResult(platform);
            }

            @Override
            public void onError(SharePlatform platform, Throwable throwable) {
                callBack.onError(platform, throwable);
                mShareCallBack.onError(platform, throwable);
            }

            @Override
            public void onCancel(SharePlatform platform) {
                callBack.onCancel(platform);
                mShareCallBack.onCancel(platform);
            }
        }, new Interceptor() {
            @Override
            public boolean allowDownloadImage() {
                shareContent.getMediaData();
                return true;
            }
        });
    }

    static IShareCallBack mShareCallBack=new IShareCallBack() {
        @Override
        public void onStart(SharePlatform var1) {

        }

        @Override
        public void onResult(SharePlatform var1) {

        }

        @Override
        public void onError(SharePlatform var1, Throwable var2) {

        }

        @Override
        public void onCancel(SharePlatform var1) {

        }
    };
}
