package com.yanhui.qktx;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.umeng.socialize.UMShareAPI;

import net.qktianxia.component.logger.proveder.Logger;
import net.qktianxia.component.share.base.IShareCallBack;
import net.qktianxia.component.share.base.SharePlatform;
import net.qktianxia.component.share.base.model.IMediaData;
import net.qktianxia.component.share.base.IShareContentProvider;
import net.qktianxia.component.share.base.model.ImageData;
import net.qktianxia.component.share.base.model.WebPageData;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {


    Logger logger=Logger.newBuilder().build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn_shareQQ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toShare(SharePlatform.QQ_SESSION);
            }
        });

        findViewById(R.id.btn_shareQZone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toShare(SharePlatform.QQ_ZONE);
            }
        });

        findViewById(R.id.btn_shareSina).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toShare(SharePlatform.SINA);
            }
        });


        findViewById(R.id.btn_shareWechatSession).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toShare(SharePlatform.WX_SESSION);
            }
        });

        findViewById(R.id.btn_shareWechatTimeline).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toShare(SharePlatform.WX_TIMELINE);
            }
        });

//
//        if(Build.VERSION.SDK_INT>=23){
//            String[] mPermissionList = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.CALL_PHONE,Manifest.permission.READ_LOGS,Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.SET_DEBUG_APP,Manifest.permission.SYSTEM_ALERT_WINDOW,Manifest.permission.GET_ACCOUNTS,Manifest.permission.WRITE_APN_SETTINGS};
//            ActivityCompat.requestPermissions(this,mPermissionList,123);
//        }

    }

    private void toShare(SharePlatform platform){
        ShareCenter.toShare(this, content, platform, new IShareCallBack() {
            @Override
            public void onStart(SharePlatform platform) {
                logger.e("onStart:"+platform.name());
            }

            @Override
            public void onResult(SharePlatform platform) {
                logger.e("onResult:"+platform.name());
            }

            @Override
            public void onError(SharePlatform platform, Throwable throwable) {
                logger.e("onError:"+platform.name()+"   throwable="+throwable.getMessage());
            }

            @Override
            public void onCancel(SharePlatform platform) {
                logger.e("onCancel:"+platform.name());
            }
        });

    }


    IShareContentProvider content=new IShareContentProvider() {
        @Override
        public String getTitle() {
            return "趣看天下";
        }

        @Override
        public String getDescription() {
            return "";
        }

        @Override
        public IMediaData getMediaData() {
            Bitmap bmp = BitmapFactory.decodeResource(getResources(),R.drawable.super_run_car);

            ByteArrayOutputStream baos= new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            String base64Text=null;
            try {
                baos.flush();
                baos.close();
                byte[] bitmapBytes = baos.toByteArray();
                base64Text = Base64.encodeToString(bitmapBytes, Base64.DEFAULT);
            } catch (IOException e) {
                e.printStackTrace();
            }

           // WebPageData webPageData=new WebPageData("https://mini.eastday.com/a/180831192927863.html?qid=null&btype=index_jrdftt&subtype=toutiao&rcgid=a74753f98c2401a1&pgnum=1&idx=1&ishot=0&recommendtype=-1&suptop=0&domain=mini&listSoftName=mini&btype1=index_jrdftt&positionxy=320,44");
            ImageData imageData=new ImageData(MainActivity.this,"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1539078787888&di=0d65120eac3837ece2737415eb801989&imgtype=0&src=http%3A%2F%2Fpic31.nipic.com%2F20130701%2F8952533_132918988000_2.jpg");
            //webPageData.setImageData(imageData);
            return imageData;
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode,requestCode,data);
        //logger.e("onActivityResult"+data.getData()+ (Constants.REQUEST_QQ_SHARE==requestCode)+"  resultCode="+(resultCode==RESULT_OK));

    }
}
