package com.yanhui.qktx;

import android.app.Application;
import android.os.Build;
import android.os.StrictMode;


import com.umeng.commonsdk.UMConfigure;

import net.qktianxia.component.share.base.ShareConfigure;

/**
 * Created by xiaolong on 2018/9/1.
 * email：xinxiaolong123@foxmail.com
 */

public class MyApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            builder.detectFileUriExposure();
        }
        ShareConfig.init(this);
    }
}
