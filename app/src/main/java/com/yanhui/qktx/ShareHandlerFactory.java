package com.yanhui.qktx;

import net.qktianxia.component.share.base.IShareHandler;
import net.qktianxia.component.share.base.SharePlatform;
import net.qktianxia.component.share.qq.QQShareHandler;
import net.qktianxia.component.share.sina.SinaShareHandler;
import net.qktianxia.component.share.wechat.WechatShareHandler;

/**
 * Created by xiaolong on 2018/9/2.
 * email：xinxiaolong123@foxmail.com
 */

public class ShareHandlerFactory {

    public static IShareHandler create(SharePlatform platform){

        IShareHandler shareHandler=null;
        switch (platform){
            case WX_SESSION:
                shareHandler=new WechatShareHandler();
                break;
            case WX_TIMELINE:
                shareHandler=new WechatShareHandler();
                break;
            case QQ_SESSION:
                shareHandler=new QQShareHandler();
                break;
            case QQ_ZONE:
                shareHandler=new QQShareHandler();
                break;
            case SINA:
                shareHandler=new SinaShareHandler();
                break;
        }

        return shareHandler;
    }

}
