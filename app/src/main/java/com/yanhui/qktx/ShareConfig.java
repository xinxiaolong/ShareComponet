package com.yanhui.qktx;

import android.content.Context;

import com.umeng.commonsdk.UMConfigure;

import net.qktianxia.component.share.base.ShareConfigure;
import net.qktianxia.component.share.qq.QQShareConfig;
import net.qktianxia.component.share.sina.SinaShareConfig;
import net.qktianxia.component.share.wechat.WechatShareConfig;

/**
 * Created by xiaolong on 2018/9/2.
 * email：xinxiaolong123@foxmail.com
 */

public class ShareConfig {

    public static final String UM_APP_ID = "59a50d09c895763cc10007e6";
    public static final String UM_CHANNEL_ID = "home";
    public static final int UM_PHONE_TYPE = UMConfigure.DEVICE_TYPE_PHONE;
    public static final String UM_PUSH_SECRET = "de8d62723cdb071751769f43347e40ca";


    public static final String QQ_APP_ID = "1106281047";
    public static final String QQ_APP_SECRET = "ediK4CzU9mjRaM0b";

    public static final String SINA_APP_ID = "277641204";
    public static final String SINA_APP_SECRET = "7e297049060e22d5fd1e563cd3664f28";
    public static final String SINA_URL_CALLBACK = "http://sns.whalecloud.com";


    public static final String WX_APP_ID = "wx1d2c2878b180942c";
    public static final String WX_APP_SECRET = "4cc9c900ccdd840277c21a0be4680703";

    public static void init(Context context) {

        ShareConfigure.initUMSDK(context, UM_APP_ID, UM_CHANNEL_ID, UM_PHONE_TYPE, UM_PUSH_SECRET);
        ShareConfigure.setDebug(true);

        QQShareConfig.setConfig(QQ_APP_ID, QQ_APP_SECRET);
        SinaShareConfig.setConfig(SINA_APP_ID, SINA_APP_SECRET, SINA_URL_CALLBACK);
        WechatShareConfig.setConfig(WX_APP_ID, WX_APP_SECRET);
    }
}
