package net.qktianxia.component.share.base;

import android.app.Activity;


/**
 * Created by xiaolong on 2018/8/31.
 * email：xinxiaolong123@foxmail.com
 */

public interface IShareHandler {

    void toShare(Activity context, SharePlatform platform, IShareContentProvider shareContent, final IShareCallBack callBack,Interceptor interceptor);
}
