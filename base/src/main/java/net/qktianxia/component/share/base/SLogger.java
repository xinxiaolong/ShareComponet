package net.qktianxia.component.share.base;

import android.support.annotation.Nullable;

import net.qktianxia.component.logger.orhanobut.logcat.AndroidLogProxy;
import net.qktianxia.component.logger.proveder.Logger;

/**
 * Created by xiaolong on 2018/9/2.
 * email：xinxiaolong123@foxmail.com
 */

public class SLogger{

    static Logger logger=Logger.newBuilder().addLogProxy(new AndroidLogProxy(){
        @Override
        public boolean isLoggable(int priority, @Nullable String tag) {
            return ShareConfigure.isDebug;
        }
    }).build();

    public static Logger get(){
        return logger;
    }

}
