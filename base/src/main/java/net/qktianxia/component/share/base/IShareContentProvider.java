package net.qktianxia.component.share.base;

import net.qktianxia.component.share.base.model.IMediaData;

/**
 * Created by xiaolong on 2018/8/31.
 * email：xinxiaolong123@foxmail.com
 */

public interface IShareContentProvider {

    String getTitle();

    String getDescription();

    IMediaData getMediaData();

}
