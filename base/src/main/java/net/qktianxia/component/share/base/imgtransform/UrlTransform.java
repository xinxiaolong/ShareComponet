package net.qktianxia.component.share.base.imgtransform;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import net.qktianxia.component.share.base.SLogger;
import net.qktianxia.component.share.base.imgtransform.base.BaseTransform;
import net.qktianxia.component.share.base.imgtransform.base.ImageTransformCallBack;

/**
 * Created by xiaolong on 2018/8/31.
 * email：xinxiaolong123@foxmail.com
 */

public class UrlTransform extends BaseTransform {

    String url;
    public UrlTransform(Context context, String url){
        super(context);
        this.url=url;
    }


    @Override
    public String asUrl(ImageTransformCallBack callBack) {
        return url;
    }

    @Override
    public byte[] asBinary(ImageTransformCallBack callBack) {
        return new byte[0];
    }

    @Override
    public Bitmap asBitmap(ImageTransformCallBack callBack) {
        if(callBack!=null)
        asBitmapCommon(Glide.with(context).asBitmap().load(url),callBack);
        return null;
    }

}
