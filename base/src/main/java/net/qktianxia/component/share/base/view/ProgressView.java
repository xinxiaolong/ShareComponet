package net.qktianxia.component.share.base.view;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by xiaolong on 2018/9/11.
 * email：xinxiaolong123@foxmail.com
 */

public class ProgressView extends ProgressDialog implements IProgressBar{
    public ProgressView(Context context) {
        super(context);
    }

    public ProgressView(Context context, int theme) {
        super(context, theme);
    }

    @Override
    public void setTitle(String title) {
        super.setTitle(title);
    }

    @Override
    public void show(String text) {
        setTitle(text);
        show();
    }
}
