package net.qktianxia.component.share.base.model;

/**
 * Created by xiaolong on 2018/8/31.
 * email：xinxiaolong123@foxmail.com
 */

public interface IMediaData {


    ImageData.MediaType getMediaType();


    public enum MediaType {
        IMAGE {
            public String toString() {
                return "0";
            }
        },
        VEDIO {
            public String toString() {
                return "1";
            }
        },
        MUSIC {
            public String toString() {
                return "2";
            }
        },
        TEXT {
            public String toString() {
                return "3";
            }
        },
        TEXT_IMAGE {
            public String toString() {
                return "4";
            }
        },
        WEBPAGE {
            public String toString() {
                return "5";
            }
        };

        private MediaType() {
        }
    }

}
