package net.qktianxia.component.share.base.imgtransform;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Base64;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import net.qktianxia.component.share.base.imgtransform.base.BaseTransform;
import net.qktianxia.component.share.base.imgtransform.base.ImageTransformCallBack;

/**
 * Created by xiaolong on 2018/9/4.
 * email：xinxiaolong123@foxmail.com
 */

public class Base64Transform extends BaseTransform {

    private String base64Text;


    public Base64Transform(Context context,String base64Text){
        super(context);
        this.base64Text=base64Text;
    }

    @Override
    public byte[] asBinary(ImageTransformCallBack callBack) {
        byte[] bytes=Base64.decode(base64Text, Base64.DEFAULT);
        if(callBack!=null)
        callBack.callBack(bytes);
        return  bytes;
    }

    @Override
    public Bitmap asBitmap(final ImageTransformCallBack callBack) {
        if(callBack!=null)
            asBitmapCommon(Glide.with(context).asBitmap().load(asBinary(null)),callBack);
        return null;
    }


    @Override
    public int asResId() {
        return 0;
    }
}
