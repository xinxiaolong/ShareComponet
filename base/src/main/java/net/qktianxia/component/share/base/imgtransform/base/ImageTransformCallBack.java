package net.qktianxia.component.share.base.imgtransform.base;

import android.graphics.Bitmap;

import java.io.File;

/**
 * Created by xiaolong on 2018/9/2.
 * email：xinxiaolong123@foxmail.com
 */

public interface ImageTransformCallBack<ImageSource> {

    void callBack(ImageSource source);
    void onFail(Exception e);

}
