package net.qktianxia.component.share.base.imgtransform;

import android.content.Context;
import android.graphics.Bitmap;


import com.bumptech.glide.Glide;

import net.qktianxia.component.share.base.imgtransform.base.BaseTransform;
import net.qktianxia.component.share.base.imgtransform.base.ImageTransformCallBack;

import java.io.File;

/**
 * Created by xiaolong on 2018/8/31.
 * email：xinxiaolong123@foxmail.com
 */

public class FileTransform extends BaseTransform {

    File file;

    public FileTransform(Context context, File file) {
        super(context);
        this.file = file;
    }

    @Override
    public File asFile(ImageTransformCallBack callBack) {
        if(callBack!=null){
            callBack.callBack(file);
        }
        return file;
    }


    @Override
    public byte[] asBinary(ImageTransformCallBack callBack) {
        return new byte[0];
    }

    @Override
    public Bitmap asBitmap(final ImageTransformCallBack callBack) {
        if(callBack!=null)
        asBitmapCommon(Glide.with(context).asBitmap().load(file),callBack);
        return null;
    }

    @Override
    public int asResId() {
        return 0;
    }

}
