package net.qktianxia.component.share.base.model;


import android.content.Context;
import android.graphics.Bitmap;


import net.qktianxia.component.share.base.imgtransform.Base64Transform;
import net.qktianxia.component.share.base.imgtransform.base.IImageTransform;
import net.qktianxia.component.share.base.imgtransform.BinaryTransform;
import net.qktianxia.component.share.base.imgtransform.BitmapTransform;
import net.qktianxia.component.share.base.imgtransform.FileTransform;
import net.qktianxia.component.share.base.imgtransform.base.ImageTransformCallBack;
import net.qktianxia.component.share.base.imgtransform.ResIdTransform;
import net.qktianxia.component.share.base.imgtransform.UrlTransform;

import java.io.File;

/**
 * Created by xiaolong on 2018/8/31.
 * email：xinxiaolong123@foxmail.com
 */

public class ImageData implements IMediaData {

    private ImageType imageType;
    private IImageTransform imageTransform;

    public enum ImageType {
        URL,
        FILE,
        RES_ID,
        BINARY,
        BITMAP,
        BASE64;
    }

    @Override
    public MediaType getMediaType() {
        return MediaType.IMAGE;
    }

    public ImageData(Context context, String url) {
        if(url.startsWith("http")){
            imageType = ImageType.URL;
        }else {
            imageType = ImageType.BASE64;
        }
        ImageData(imageType, context, url);
    }

    public ImageData(Context context, File file) {
        imageType = ImageType.FILE;
        ImageData(imageType, context, file);
    }

    public ImageData(Context context, int resId) {
        imageType = ImageType.RES_ID;
        ImageData(imageType, context, resId);
    }

    public ImageData(Context context, byte[] bytes) {
        imageType = ImageType.BINARY;
        ImageData(imageType, context, bytes);
    }

    public ImageData(Context context, Bitmap bitmap) {
        imageType = ImageType.BITMAP;
        ImageData(imageType, context, bitmap);
    }

    private void ImageData(ImageType imageType, Context context, Object object) {
        switch (imageType) {
            case URL:
                imageTransform = new UrlTransform(context, String.valueOf(object));
                break;
            case FILE:
                imageTransform = new FileTransform(context, (File) object);
                break;
            case RES_ID:
                imageTransform = new ResIdTransform(context, (int) object);
                break;
            case BINARY:
                imageTransform = new BinaryTransform(context, (byte[]) object);
                break;
            case BITMAP:
                imageTransform = new BitmapTransform(context, (Bitmap) object);
                break;
            case BASE64:
                imageTransform=new Base64Transform(context,String.valueOf(object));
                break;
        }
    }

    public ImageType getImageType() {
        return imageType;
    }

    public void setImageTransform(IImageTransform imageTransform) {
        this.imageTransform = imageTransform;
    }


    public File asFile(ImageTransformCallBack<File> callBack) {
        if (getImageType() != ImageType.FILE && callBack == null) {
            RuntimeException exception = new RuntimeException("必须使用异步回调");
            throw exception;
        }
        return imageTransform.asFile(callBack);
    }

    public String asUrl(ImageTransformCallBack<String> callBack) {
        if (getImageType() != ImageType.URL && callBack == null) {
            RuntimeException exception = new RuntimeException("必须使用异步回调");
            throw exception;
        }
        return imageTransform.asUrl(callBack);
    }

    public byte[] asBinary(ImageTransformCallBack<byte[]> callBack) {
        if (getImageType() != ImageType.BINARY && callBack == null) {
            RuntimeException exception = new RuntimeException("必须使用异步回调");
            throw exception;
        }
        return imageTransform.asBinary(callBack);
    }

    public int asResId() {
        return imageTransform.asResId();
    }


    public Bitmap asBitmap(ImageTransformCallBack<Bitmap> callBack) {
        if (getImageType() != ImageType.BITMAP && callBack == null) {
            RuntimeException exception = new RuntimeException("必须使用异步回调");
            throw exception;
        }
        return imageTransform.asBitmap(callBack);
    }

}
