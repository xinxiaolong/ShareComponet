package net.qktianxia.component.share.base.imgtransform;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import net.qktianxia.component.share.base.SLogger;
import net.qktianxia.component.share.base.imgtransform.base.BaseTransform;
import net.qktianxia.component.share.base.imgtransform.base.ImageTransformCallBack;

/**
 * Created by xiaolong on 2018/8/31.
 * email：xinxiaolong123@foxmail.com
 */

public class ResIdTransform extends BaseTransform {


    int resId;
    Context context;

    public ResIdTransform(Context context, int resId){
        super(context);
        this.context=context;
        this.resId=resId;
    }


    @Override
    public byte[] asBinary(ImageTransformCallBack callBack) {
        return new byte[0];
    }

    @Override
    public Bitmap asBitmap(final ImageTransformCallBack callBack) {
        Glide.with(context).asBitmap().load(resId).addListener(new RequestListener<Bitmap>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                SLogger.get().e(e,"");
                return false;
            }

            @Override
            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                callBack.callBack(resource);
                return false;
            }
        }).submit();
        return null;
    }

    @Override
    public int asResId() {
        return resId;
    }
}
