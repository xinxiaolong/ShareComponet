package net.qktianxia.component.share.base;

import android.content.Context;

import com.umeng.commonsdk.UMConfigure;
import com.umeng.socialize.PlatformConfig;

/**
 * Created by xiaolong on 2018/9/2.
 * email：xinxiaolong123@foxmail.com
 */

public class ShareConfigure {

    public static boolean isDebug=false;

    public static void initUMSDK(Context context, String appKey, String channel, int deviceType, String pushSecret){
        UMConfigure.init(context, appKey, channel, deviceType,pushSecret);
    }

    public static void setDebug(boolean isDebug){
        ShareConfigure.isDebug=isDebug;
        UMConfigure.setLogEnabled(isDebug);
    }

    public static void setQQConfig(String key,String appSecret){
        PlatformConfig.setQQZone(key, appSecret);
    }


    public static void setSinaConfig(String key,String appSecret,String callBackUrl){
        PlatformConfig.setSinaWeibo(key, appSecret,callBackUrl);

    }
}
