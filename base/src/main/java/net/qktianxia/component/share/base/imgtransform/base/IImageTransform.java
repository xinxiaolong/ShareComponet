package net.qktianxia.component.share.base.imgtransform.base;

import android.graphics.Bitmap;

import net.qktianxia.component.share.base.imgtransform.base.ImageTransformCallBack;

import java.io.File;

/**
 * Created by xiaolong on 2018/8/31.
 * email：xinxiaolong123@foxmail.com
 */

public interface IImageTransform {

    File asFile(ImageTransformCallBack<File> callBack);

    String asUrl(ImageTransformCallBack<String> callBack);

    byte[] asBinary(ImageTransformCallBack<byte[]> callBack);

    Bitmap asBitmap(ImageTransformCallBack<Bitmap> callBack);

    int asResId();
}
