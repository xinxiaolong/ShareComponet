package net.qktianxia.component.share.base;

/**
 * Created by xiaolong on 2018/8/31.
 * email：xinxiaolong123@foxmail.com
 */

public interface IShareCallBack {

    void onStart(SharePlatform platform);

    void onResult(SharePlatform platform);

    void onError(SharePlatform platform, Throwable throwable);

    void onCancel(SharePlatform platform);
}
