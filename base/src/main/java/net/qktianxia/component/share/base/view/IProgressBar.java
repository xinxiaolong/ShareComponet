package net.qktianxia.component.share.base.view;

/**
 * Created by xiaolong on 2018/9/11.
 * email：xinxiaolong123@foxmail.com
 */

public interface IProgressBar {

    void setTitle(String title);

    void show();

    void show(String title);

    void cancel();
}
