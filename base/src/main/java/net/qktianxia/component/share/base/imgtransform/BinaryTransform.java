package net.qktianxia.component.share.base.imgtransform;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import net.qktianxia.component.share.base.SLogger;
import net.qktianxia.component.share.base.imgtransform.base.BaseTransform;
import net.qktianxia.component.share.base.imgtransform.base.ImageTransformCallBack;

/**
 * Created by xiaolong on 2018/8/31.
 * email：xinxiaolong123@foxmail.com
 */

public class BinaryTransform extends BaseTransform {


    byte[] bytes;

    public BinaryTransform(Context context, byte[] bytes){
        super(context);
        this.bytes=bytes;
    }

    @Override
    public byte[] asBinary(ImageTransformCallBack callBack) {
        return bytes;
    }

    @Override
    public Bitmap asBitmap(final ImageTransformCallBack callBack) {
        if(callBack!=null)
        asBitmapCommon(Glide.with(context).asBitmap().load(bytes),callBack);
        return null;
    }

    @Override
    public int asResId() {
        return 0;
    }

}
