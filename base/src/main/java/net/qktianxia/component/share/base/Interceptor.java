package net.qktianxia.component.share.base;

/**
 * Created by xiaolong on 2018/10/9.
 * email：xinxiaolong123@foxmail.com
 */

public interface Interceptor {

     boolean allowDownloadImage();

}
