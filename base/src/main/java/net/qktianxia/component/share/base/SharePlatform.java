package net.qktianxia.component.share.base;

/**
 * Created by xiaolong on 2018/8/31.
 * email：xinxiaolong123@foxmail.com
 *
 * 微信：https://open.weixin.qq.com/cgi-bin/showdocument?action=dir_list&t=resource/res_list&verify=1&id=open1419317340&token=2d32abcef8d40a23d0d2a341fa52104b2c3a188c&lang=zh_CN
 * QQ：http://wiki.open.qq.com/index.php?title=Android_API%E8%B0%83%E7%94%A8%E8%AF%B4%E6%98%8E&=45038
 * 新浪微博：https://github.com/sinaweibosdk/weibo_android_sdk/blob/master/%E6%96%B0%E6%96%87%E6%A1%A3/%E5%BE%AE%E5%8D%9ASDK%204.1%E6%96%87%E6%A1%A3.pdf
 *
 *
 */

public enum SharePlatform {
    WX_SESSION,
    WX_TIMELINE,
    QQ_SESSION,
    QQ_ZONE,
    SINA;
}
