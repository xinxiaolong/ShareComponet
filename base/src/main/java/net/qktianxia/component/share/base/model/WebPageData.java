package net.qktianxia.component.share.base.model;

/**
 * Created by xiaolong on 2018/8/31.
 * email：xinxiaolong123@foxmail.com
 */

public class WebPageData implements IMediaData{


    private String webUrl;
    private ImageData imageData;

    @Override
    public MediaType getMediaType() {
        return MediaType.WEBPAGE;
    }

    public WebPageData(String webUrl){
        this.webUrl=webUrl;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public ImageData getImageData() {
        return imageData;
    }

    public void setImageData(ImageData imageData) {
        this.imageData = imageData;
    }

}
