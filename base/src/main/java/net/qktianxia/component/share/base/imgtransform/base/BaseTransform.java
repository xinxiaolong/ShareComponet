package net.qktianxia.component.share.base.imgtransform.base;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;

import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import net.qktianxia.component.share.base.SLogger;
import net.qktianxia.component.share.base.Util;
import net.qktianxia.component.share.base.view.IProgressBar;
import net.qktianxia.component.share.base.view.ProgressHolder;

import java.io.File;

/**
 * Created by xiaolong on 2018/9/4.
 * email：xinxiaolong123@foxmail.com
 * 基础的图片转换器。
 * 实现了基本的 asFile() 和 asBitmap() 方法
 */

public abstract class BaseTransform implements IImageTransform {

    public Handler handler;
    public Context context;
    public IProgressBar progressBar;


    public BaseTransform(Context context) {
        this.context = context;
        handler = new Handler(Looper.getMainLooper());
        progressBar = ProgressHolder.create(context);

    }

    @Override
    public File asFile(final ImageTransformCallBack callBack) {
        if (callBack != null)
            asFileCommon(callBack);
        return null;
    }


    private void asFileCommon(final ImageTransformCallBack<File> callBack) {
        progressBar.show();
        asBitmap(new ImageTransformCallBack<Bitmap>() {
            @Override
            public void callBack(final Bitmap bitmap) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final File file = Util.saveImageToFile(context, bitmap);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.cancel();
                                callBack.callBack(file);
                            }
                        });
                    }
                }).start();

            }

            @Override
            public void onFail(Exception e) {
                progressBar.cancel();
                SLogger.get().e(e, "");
            }
        });
    }

    @Override
    public String asUrl(ImageTransformCallBack<String> callBack) {
        return null;
    }

    @Override
    public byte[] asBinary(ImageTransformCallBack<byte[]> callBack) {
        return new byte[0];
    }

    @Override
    public abstract Bitmap asBitmap(ImageTransformCallBack<Bitmap> callBack);


    public void asBitmapCommon(RequestBuilder<Bitmap> glideRequest, final ImageTransformCallBack callBack) {
        progressBar.show();
        glideRequest.addListener(new RequestListener<Bitmap>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                callBack.onFail(e);
                progressBar.cancel();
                return false;
            }

            @Override
            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                callBack.callBack(resource);
                progressBar.cancel();
                return false;
            }
        }).submit();
    }


    @Override
    public int asResId() {
        return 0;
    }
}
