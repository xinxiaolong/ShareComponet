package net.qktianxia.component.share.base.imgtransform;

import android.content.Context;
import android.graphics.Bitmap;

import net.qktianxia.component.share.base.imgtransform.base.BaseTransform;
import net.qktianxia.component.share.base.imgtransform.base.ImageTransformCallBack;

import java.io.ByteArrayOutputStream;

/**
 * Created by xiaolong on 2018/8/31.
 * email：xinxiaolong123@foxmail.com
 */

public class BitmapTransform extends BaseTransform {

    private Bitmap bitmap;


    public BitmapTransform(Context context, Bitmap bitmap){
        super(context);
        this.bitmap=bitmap;
    }

    @Override
    public byte[] asBinary(ImageTransformCallBack callBack) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);

        byte[] bytes=baos.toByteArray();
        if(callBack!=null){
            callBack.callBack(bytes);
        }
        return bytes;
    }

    @Override
    public Bitmap asBitmap(ImageTransformCallBack callBack) {
        if(callBack!=null){
            callBack.callBack(bitmap);
        }
        return bitmap;
    }

    @Override
    public int asResId() {
        return 0;
    }
}
