package net.qktianxia.component.share.base.view;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by xiaolong on 2018/9/11.
 * email：xinxiaolong123@foxmail.com
 */

public class ProgressHolder {
    public static IProgressBar create(Context context){
        IProgressBar progressBar=new ProgressView(context,ProgressDialog.STYLE_SPINNER);
        progressBar.setTitle("正在加载...");
        return progressBar;
    }
}
