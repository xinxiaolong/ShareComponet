package net.qktianxia.component.share.qq;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.widget.Toast;

import net.qktianxia.component.share.base.IShareCallBack;
import net.qktianxia.component.share.base.IShareContentProvider;
import net.qktianxia.component.share.base.IShareHandler;
import net.qktianxia.component.share.base.Interceptor;
import net.qktianxia.component.share.base.SharePlatform;

/**
 * Created by xiaolong on 2018/9/5.
 * email：xinxiaolong123@foxmail.com
 */

public class IntentShareHandler implements IShareHandler{


    @Override
    public void toShare(Activity context, SharePlatform platform, IShareContentProvider shareContent, IShareCallBack callBack,Interceptor interceptor) {
        throughIntentShareQQDesc(context,shareContent.getTitle()+"  "+shareContent.getDescription());
    }


    /**
     * 通过intent 分享内容到QQ
     *
     * @param desc
     */
    public static void throughIntentShareQQDesc(Activity context,String desc) {
        try {
            Intent intent = new Intent();
            ComponentName componentFirend = new ComponentName("com.tencent.mobileqq", "com.tencent.mobileqq.activity.JumpActivity");
            intent.setComponent(componentFirend);
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("text/*");
            intent.putExtra(Intent.EXTRA_TEXT, desc);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(intent);
        } catch (Exception e) {
            if (e.toString().contains("android.content.ActivityNotFoundException")) {
                Toast.makeText(context, "未发现微信", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
