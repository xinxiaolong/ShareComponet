package net.qktianxia.component.share.qq;

import com.umeng.socialize.PlatformConfig;

/**
 * Created by xiaolong on 2018/9/2.
 * email：xinxiaolong123@foxmail.com
 */

public class QQShareConfig {

    public static void setConfig(String key,String appSecret){
        PlatformConfig.setQQZone(key, appSecret);
    }
}
