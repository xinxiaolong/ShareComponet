package net.qktianxia.component.share.sina;

import com.umeng.socialize.PlatformConfig;

/**
 * Created by xiaolong on 2018/9/2.
 * email：xinxiaolong123@foxmail.com
 */

public class SinaShareConfig {

    public static void setConfig(String key,String appSecret,String callBackUrl){
        PlatformConfig.setSinaWeibo(key,appSecret,callBackUrl);
    }
}
