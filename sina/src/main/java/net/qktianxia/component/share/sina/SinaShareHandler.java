package net.qktianxia.component.share.sina;

import android.app.Activity;

import com.umeng.socialize.ShareAction;
import com.umeng.socialize.ShareContent;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.BaseMediaObject;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;

import net.qktianxia.component.share.base.IShareCallBack;
import net.qktianxia.component.share.base.IShareHandler;
import net.qktianxia.component.share.base.Interceptor;
import net.qktianxia.component.share.base.SLogger;
import net.qktianxia.component.share.base.SharePlatform;
import net.qktianxia.component.share.base.imgtransform.base.ImageTransformCallBack;
import net.qktianxia.component.share.base.model.IMediaData;
import net.qktianxia.component.share.base.IShareContentProvider;
import net.qktianxia.component.share.base.model.ImageData;
import net.qktianxia.component.share.base.model.WebPageData;
import net.qktianxia.component.share.base.view.IProgressBar;
import net.qktianxia.component.share.base.view.ProgressHolder;

import java.io.File;
import java.lang.ref.WeakReference;

/**
 * Created by xiaolong on 2018/8/31.
 * email：xinxiaolong123@foxmail.com
 */

public class SinaShareHandler implements IShareHandler {

    private Activity mContext;
    private IShareContentProvider shareContent;
    private IShareCallBack callBack;
    private UMWeb umWeb;
    private IProgressBar progressBar;
    private Interceptor interceptor;

    public void toShare(Activity context, SharePlatform platform, IShareContentProvider shareContent, final IShareCallBack callBack, Interceptor interceptor) {

        this.mContext = (Activity) (new WeakReference(context)).get();
        this.shareContent = shareContent;
        this.callBack = callBack;
        this.interceptor=interceptor;
        this.progressBar= ProgressHolder.create(context);
        progressBar.setTitle("正在打开微博...");
        parseAndShare(shareContent);
    }


    private void parseAndShare(IShareContentProvider shareContent) {

        IMediaData mediaData = shareContent.getMediaData();
        if(mediaData==null){
            shareText();
            return;
        }

        switch (mediaData.getMediaType()) {
            case IMAGE:
                getUMImageAndShare((ImageData)mediaData,null);
                break;
            case WEBPAGE:
                WebPageData webPageData=(WebPageData)mediaData;
                umWeb=new UMWeb(webPageData.getWebUrl());

                getUMImageAndShare(webPageData.getImageData(),umWeb);
                break;
            default:
                shareToUm(mContext, mapTo(shareContent, null), callBack);
                break;
        }
    }

    private void shareText(){
        ShareContent umShareContent=new ShareContent();
        umShareContent.mText=shareContent.getTitle();
        umShareContent.subject=shareContent.getDescription();
        shareToUm(mContext,umShareContent ,callBack);
    }

    private void getUMImageAndShare(ImageData imageData,UMWeb umWeb) {
        if (imageData == null) {
            shareToUm(mContext, mapTo(shareContent, umWeb), callBack);
        } else {
            switch (imageData.getImageType()) {
                case URL:
                    UMImage umImage=new UMImage(mContext,imageData.asUrl(null));
                    if(umWeb!=null){
                        umWeb.setThumb(umImage);
                        shareToUm(mContext, mapTo(shareContent, umWeb), callBack);
                    }else {
                        shareToUm(mContext, mapTo(shareContent, umImage), callBack);
                    }
                    break;
                default:
                    loadImageFileAndShare(imageData,umWeb);
                    break;
            }
        }
    }

    private void loadImageFileAndShare(final ImageData imageData, final UMWeb umWeb){

        if(!interceptor.allowDownloadImage()){
            return;
        }

        imageData.asFile(new ImageTransformCallBack<File>() {
            @Override
            public void callBack(File file) {

                UMImage umImage=new UMImage(mContext,file);

                if(umWeb!=null){
                    umWeb.setThumb(umImage);
                    shareToUm(mContext, mapTo(shareContent, umWeb), callBack);
                }else {
                    shareToUm(mContext, mapTo(shareContent, umImage), callBack);
                }
            }

            @Override
            public void onFail(Exception e) {
                SLogger.get().e(e,"");
            }
        });
    }



    private ShareContent mapTo(IShareContentProvider shareContent, BaseMediaObject umData) {
        ShareContent umContent = new ShareContent();

        umData.setTitle(shareContent.getTitle());
        umData.setDescription(shareContent.getDescription());
        umContent.mText = shareContent.getTitle();
        umContent.subject = shareContent.getDescription();

        if (umData != null)
            umContent.mMedia = umData;

        return umContent;
    }


    private void shareToUm(Activity context, ShareContent shareContent, final IShareCallBack callBack) {
        new ShareAction(context).setPlatform(SHARE_MEDIA.SINA).setShareContent(shareContent).setCallback(new UMShareListener() {
            @Override
            public void onStart(SHARE_MEDIA share_media) {
                progressBar.show();
                callBack.onStart(SharePlatform.SINA);
            }

            @Override
            public void onResult(SHARE_MEDIA share_media) {
                callBack.onResult(SharePlatform.SINA);
                progressBar.cancel();
            }

            @Override
            public void onError(SHARE_MEDIA share_media, Throwable throwable) {
                callBack.onError(SharePlatform.SINA, throwable);
                progressBar.cancel();
            }

            @Override
            public void onCancel(SHARE_MEDIA share_media) {
                callBack.onCancel(SharePlatform.SINA);
                progressBar.cancel();
            }
        }).share();
    }


}
