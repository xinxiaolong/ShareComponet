package net.qktianxia.component.share.wechat.wrshare;

/**
 * Created by liupanpan on 2017/8/28.
 */

public class ShareThirdConstant {

    public static final String WEIXINAPPKEYQQ;
    public static final String WEIXINAPPPACKAGEQQ;

    public static final String WEIXINAPPKEYUC;
    public static final String WEIXINAPPPACKAGEUC;

    public static final String WEIXINAPPKEYQQBROWSER;
    public static final String WEIXINAPPPACKAGEQQBROWSER;

    public static final String WEIXINAPPKEYSINA;
    public static final String WEIXINAPPPACKAGESINA;

    public static final String WEIXINAPPKEYNEWSTODAY;
    public static final String WEIXINAPPPACKAGENEWSTODAY;

    public static final String WEIXINAPPKEYBAIDU;
    public static final String WEIXINAPPPACKAGEBAIDU;

    public static final String TENTXUNNEWSAPPKEY;
    public static final String TENTXUNNEWSPAGER;

    public static final String WEIXINAPPKEYDAYANDDAY;
    public static final String WEIXINAPPPACKAGEDAYANDDAY;

    public static final String TENTXUNYINGYONGBAOSAPPKEY;
    public static final String TENTXUNYINGYONGBAOSPAGER;


    static {
        /**
         * qq的appId ,package
         */
        WEIXINAPPKEYQQ = "wxf0a80d0ac2e82aa7";
        WEIXINAPPPACKAGEQQ = "com.tencent.mobileqq";

        /**
         * UC
         */
        WEIXINAPPKEYUC = "wx020a535dccd46c11";
        WEIXINAPPPACKAGEUC = "com.UCMobile";

        /**
         * QQ浏览器
         */
        WEIXINAPPKEYQQBROWSER = "wx64f9cf5b17af074d";
        WEIXINAPPPACKAGEQQBROWSER = "com.tencent.mtt";

        /**
         * 微博
         */
        WEIXINAPPKEYSINA = "wx299208e619de7026";
        WEIXINAPPPACKAGESINA = "com.sina.weibo";

        /**
         * 今日头条
         */
        WEIXINAPPKEYNEWSTODAY = "wx50d801314d9eb858";
        WEIXINAPPPACKAGENEWSTODAY = "com.ss.android.article.news";

        /**
         * 百度
         */
        WEIXINAPPKEYBAIDU = "wx27a43222a6bf2931";
        WEIXINAPPPACKAGEBAIDU = "com.baidu.searchbox";

        /**
         * 天天快报
         */
        WEIXINAPPKEYDAYANDDAY = "wxe90c9765ad00e2cd";
        WEIXINAPPPACKAGEDAYANDDAY = "com.tencent.reading";

        /**
         * 腾讯新闻
         */
        TENTXUNNEWSAPPKEY = "wx073f4a4daff0abe8";
        TENTXUNNEWSPAGER = "com.tencent.news";
        /**
         * 腾讯应用宝
         */
        TENTXUNYINGYONGBAOSAPPKEY = "wx3909f6add1206543";
        TENTXUNYINGYONGBAOSPAGER = "com.tencent.android.qqdownloader";
//        /**
//         * 小米浏览器
//         */
//         TENTXUNXIAOMIAPPKEY="wxc87ca23cfe029db3";
//         TENTXUNXIAOMIPAGER="com.android.browser";

    }
}
