package net.qktianxia.component.share.wechat.handler;

import android.app.Activity;
import android.text.TextUtils;

import net.qktianxia.component.share.base.IShareCallBack;
import net.qktianxia.component.share.base.Interceptor;
import net.qktianxia.component.share.base.SharePlatform;
import net.qktianxia.component.share.base.imgtransform.base.ImageTransformCallBack;
import net.qktianxia.component.share.base.model.IMediaData;
import net.qktianxia.component.share.base.IShareContentProvider;
import net.qktianxia.component.share.base.model.ImageData;
import net.qktianxia.component.share.base.model.WebPageData;
import net.qktianxia.component.share.wechat.wrshare.ShareUtils;

import java.io.File;
import java.lang.ref.WeakReference;

/**
 * Created by xiaolong on 2018/9/3.
 * email：xinxiaolong123@foxmail.com
 */

public class UseIntentShareHandler extends WRShareHandler {


    @Override
    public void toShare(Activity context, SharePlatform platform, IShareContentProvider shareContent, IShareCallBack callBack, Interceptor interceptor) {
        init(context,platform,shareContent,callBack,interceptor);
        shareOfIntent();
    }


    private void shareOfIntent() {
        IMediaData mediaData = shareContent.getMediaData();
        switch (mediaData.getMediaType()) {
            case IMAGE:
                getImageFileAndShare(getImageData(shareContent));
                break;
            default:
                ShareUtils.sharePureText(weakReference.get(), getAllContent(shareContent));
                break;
        }
    }

    private void getImageFileAndShare(ImageData imageData) {
        if (imageData == null) {
            ShareUtils.sharePureText(weakReference.get(), getAllContent(shareContent));
            return;
        }

        if(!interceptor.allowDownloadImage()){
            return;
        }
        imageData.asFile(new ImageTransformCallBack<File>() {
            @Override
            public void callBack(File file) {
                toShare(file);
            }

            @Override
            public void onFail(Exception e) {

            }
        });
    }

    private void toShare(File file){
        switch (platform){
            case WX_SESSION:
                ShareUtils.sharePureImage(weakReference.get(),file);
                break;
            case WX_TIMELINE:
                ShareUtils.shareTextAndImage(weakReference.get(), getAllContent(shareContent), file);
                break;
        }
    }

    private String getAllContent(IShareContentProvider shareContent){
        return shareContent.getTitle()+" "+shareContent.getDescription();
    }


}
