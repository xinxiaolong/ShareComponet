package net.qktianxia.component.share.wechat.handler;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;

import net.qktianxia.component.share.base.IShareCallBack;
import net.qktianxia.component.share.base.Interceptor;
import net.qktianxia.component.share.base.SLogger;
import net.qktianxia.component.share.base.SharePlatform;
import net.qktianxia.component.share.base.imgtransform.base.ImageTransformCallBack;
import net.qktianxia.component.share.base.model.IMediaData;
import net.qktianxia.component.share.base.IShareContentProvider;
import net.qktianxia.component.share.base.model.ImageData;
import net.qktianxia.component.share.base.model.WebPageData;
import net.qktianxia.component.share.wechat.wrshare.ShareUtils;

import java.io.File;
import java.lang.ref.WeakReference;

/**
 * Created by xiaolong on 2018/9/3.
 * email：xinxiaolong123@foxmail.com
 *
 * 走第三方的条件：
 * 必填项： title webUrl image 缺一不可
 */

public class UseOtherAppShareHandler extends WRShareHandler {


    String[] strings;
    String webUrl;

    public UseOtherAppShareHandler(String[] strings){
        this.strings=strings;
    }

    @Override
    public void toShare(Activity context, SharePlatform platform, IShareContentProvider shareContent, IShareCallBack callBack, Interceptor interceptor) {
        init(context,platform,shareContent,callBack,interceptor);
        shareOfOther();
    }


    private void shareOfOther() {
        IMediaData mediaData = shareContent.getMediaData();
        switch (mediaData.getMediaType()) {
            case WEBPAGE:
                WebPageData webPageData=(WebPageData)mediaData;
                webUrl=webPageData.getWebUrl();
                toShare(webPageData,webPageData.getImageData());
                break;
            default:
                SLogger.get().e("暂不支持%s类型进行微信分享",platform);
                break;
        }
    }


    private void toShare(WebPageData webPageData,ImageData imageData){
        if(imageData==null){
            ShareUtils.sharePureText(weakReference.get(),shareContent.getTitle()+"  "+shareContent.getDescription()+"  "+webPageData.getWebUrl());
            return;
        }
        imageData.asBitmap(new ImageTransformCallBack<Bitmap>() {
            @Override
            public void callBack(Bitmap bitmap) {
                int type=platform==SharePlatform.WX_SESSION?0:1;
                ShareUtils.shareWXRX(weakReference,strings[0],strings[1],shareContent.getTitle(),shareContent.getDescription(),webUrl,type,bitmap);
            }

            @Override
            public void onFail(Exception e) {

            }
        });
    }
}
