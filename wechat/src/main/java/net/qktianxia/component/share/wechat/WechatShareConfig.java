package net.qktianxia.component.share.wechat;

import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.media.UMImage;

/**
 * Created by xiaolong on 2018/9/2.
 * email：xinxiaolong123@foxmail.com
 */

public class WechatShareConfig {

    public static void setConfig(String key,String appSecret){
        PlatformConfig.setWeixin(key, appSecret);
    }
}
