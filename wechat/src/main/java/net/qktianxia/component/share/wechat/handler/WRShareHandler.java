package net.qktianxia.component.share.wechat.handler;

import android.app.Activity;

import net.qktianxia.component.share.base.IShareCallBack;
import net.qktianxia.component.share.base.IShareHandler;
import net.qktianxia.component.share.base.Interceptor;
import net.qktianxia.component.share.base.SharePlatform;
import net.qktianxia.component.share.base.model.IMediaData;
import net.qktianxia.component.share.base.IShareContentProvider;
import net.qktianxia.component.share.base.model.ImageData;
import net.qktianxia.component.share.base.model.WebPageData;
import net.qktianxia.component.share.wechat.wrshare.ShareUtils;

import java.lang.ref.WeakReference;

/**
 * Created by xiaolong on 2018/9/3.
 * email：xinxiaolong123@foxmail.com
 */

public class WRShareHandler implements IShareHandler{


    protected WeakReference<Activity> weakReference;;
    protected IShareContentProvider shareContent;
    protected SharePlatform platform;
    protected IShareCallBack callBack;
    protected Interceptor interceptor;

    @Override
    public void toShare(Activity context, SharePlatform platform, IShareContentProvider shareContent, IShareCallBack callBack, Interceptor interceptor) {
        init(context,platform,shareContent,callBack,interceptor);
        findCanShareAppAndShare();
    }

    public void init(Activity context, SharePlatform platform, IShareContentProvider shareContent, IShareCallBack callBack,Interceptor interceptor){
        weakReference=new WeakReference(context);
        this.platform=platform;
        this.shareContent=shareContent;
        this.callBack=callBack;
        this.interceptor=interceptor;
    }


    private void findCanShareAppAndShare() {

        if(shareContent.getMediaData()==null){
            ShareUtils.sharePureText(weakReference.get(),shareContent.getTitle()+" "+shareContent.getDescription());
            return;
        }

        String[] strings = ShareUtils.shareWXReadyRx(weakReference.get(), null);
        if (strings != null && shareContent.getMediaData().getMediaType()== IMediaData.MediaType.WEBPAGE) {
            //走第三方包分享，且只能分享webPage类型
            new UseOtherAppShareHandler(strings).toShare(weakReference.get(),platform,shareContent,callBack,interceptor);
        } else {
            //走Intent分享
            new UseIntentShareHandler().toShare(weakReference.get(),platform,shareContent,callBack,interceptor);
        }
    }


    protected ImageData getImageData(IShareContentProvider shareContent) {

        ImageData imageData = null;

        IMediaData mediaData = shareContent.getMediaData();
        switch (mediaData.getMediaType()) {
            case IMAGE:
                imageData = (ImageData) mediaData;
                break;
            case WEBPAGE:
                WebPageData webPageData = (WebPageData) shareContent.getMediaData();
                imageData = webPageData.getImageData();
                break;
        }
        return imageData;
    }


}
