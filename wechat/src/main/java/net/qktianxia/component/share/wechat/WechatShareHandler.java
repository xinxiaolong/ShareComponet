package net.qktianxia.component.share.wechat;

import android.app.Activity;

import net.qktianxia.component.share.base.IShareCallBack;
import net.qktianxia.component.share.base.IShareHandler;
import net.qktianxia.component.share.base.Interceptor;
import net.qktianxia.component.share.base.SharePlatform;
import net.qktianxia.component.share.base.IShareContentProvider;
import net.qktianxia.component.share.wechat.handler.WRShareHandler;

/**
 * Created by xiaolong on 2018/9/2.
 * email：xinxiaolong123@foxmail.com
 */

public class WechatShareHandler implements IShareHandler {

    @Override
    public void toShare(Activity context, SharePlatform platform, IShareContentProvider shareContent, IShareCallBack callBack, Interceptor interceptor) {
        new WRShareHandler().toShare(context,platform,shareContent,callBack,interceptor);
    }

}
